from re import fullmatch
import keyring as kr	
from requests import get

def check(password):
	data = list(get('https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10-million-password-list-top-100000.txt').text)
	if password in data:
		return "Password leak!"
	if not fullmatch(r'[A-Za-z0-9@#$%^&+=]{8,}', password):
		return "Password is too weak!"
	else:
		return "Strong password"

def test():
	password = kr.get_password("system", "denny")
	return check(password)

print("Checking local password...")
print(test)
password = input("New password: ")
print(check(password))
